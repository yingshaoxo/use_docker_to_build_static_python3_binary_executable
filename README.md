# Build static version of Python 3.10.4

The Docker image available on this repository allows to build Linux static binaries of python. 

This binaries should run fine on any amd64 Linux system (x64 only).

> We also provide amd32 or x32 or x86 or i386 arch version of python in "./data_i386" folder.

<!-- But it lacks modules: https://github.com/python/cpython/tree/main/Modules

If you want to compile python staticlly without libc, you have to install every c dependencies manually. -->

## How to get the compiled python?
```bash
sudo su
curl -sSL https://gitlab.com/yingshaoxo/use_docker_to_build_static_python3_binary_executable/-/raw/master/install.sh?ref_type=heads | bash
```

or

```bash
# This static version of python is only 50MB after: tar -cvzf small_static_python3.10.tar.gz ~/Downloads/static_python_3.10

git clone https://gitlab.com/yingshaoxo/use_docker_to_build_static_python3_binary_executable.git
cd use_docker_to_build_static_python3_binary_executable

mkdir -p ~/Documents/static_python_3.10

cp -fr data/Python-3.10.4/* ~/Documents/static_python_3.10/

cd ~/Documents/static_python_3.10
sudo chmod 777 python

sudo rm -fr /bin/python
sudo rm -fr /bin/python3
sudo ln -s ~/Documents/static_python_3.10/python /bin/python
sudo ln -s ~/Documents/static_python_3.10/python /bin/python3
sudo chmod 777 /bin/python
sudo chmod 777 /bin/python3
sudo 
sudo rm -fr /usr/bin/python
sudo rm -fr /usr/bin/python3
sudo ln -s ~/Documents/static_python_3.10/python /usr/bin/python
sudo ln -s ~/Documents/static_python_3.10/python /usr/bin/python3
sudo chmod 777 /usr/bin/python
sudo chmod 777 /usr/bin/python3

python --version
```

> All you need is `Lib`, `Modules`, `python`, `pybuilddir.txt`, `build`

## How to build python?

```
sudo docker-compose -f docker-compose.python.yaml up --build
```

If anything goes wrong, you may want to get into the docker container to fix it:
```
sudo docker exec -it --user=root 3d86fb657430 /bin/bash
```

## How to use the compiled python?

```
./data/Python-3.10.4/python auto_everything/test.py
```

or 

```
cd ./data && make altinstall
```

## Where is the python documentation for this build?
Check "./python_documentation/python-3.10.12-docs-text/glossary.txt"

## Author
yingshaoxo
