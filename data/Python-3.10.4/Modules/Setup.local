*static*

#array arraymodule.c    # array objects
#cmath cmathmodule.c _math.c # -lm # complex math library functions
#math mathmodule.c _math.c # -lm # math library functions, e.g. sin()
#_struct _struct.c      # binary structure packing/unpacking
#_random _randommodule.c        # Random number generator
#_datetime _datetimemodule.c    # datetime accelerator

#binascii binascii.c
#select selectmodule.c
#termios termios.c
#fcntl fcntlmodule.c
#resource resource.c
#_posixsubprocess _posixsubprocess.c
#_md5 md5module.c # md5.c
_uuid _uuidmodule.c -luuid
_lzma _lzmamodule.c -llzma
_multiprocessing -I$(srcdir)/Modules/_multiprocessing _multiprocessing/multiprocessing.c _multiprocessing/semaphore.c
_queue _queuemodule.c
_ctypes _ctypes/_ctypes.c _ctypes/callbacks.c _ctypes/callproc.c _ctypes/stgdict.c _ctypes/cfield.c -ldl -lffi -DHAVE_FFI_PREP_CIF_VAR -DHAVE_FFI_PREP_CLOSURE_LOC -DHAVE_FFI_CLOSURE_ALLOC
_ctypes_test _ctypes/_ctypes_test.c

_curses -lncurses -lncursesw -ltermcap _cursesmodule.c
_curses_panel -lpanel -lncurses _curses_panel.c

_bz2 _bz2module.c -lbz2
_decimal -I$(srcdir)/Modules/_decimal -I$(srcdir)/Modules/_decimal/libmpdec _decimal/libmpdec/mpdecimal.c _decimal/libmpdec/basearith.c _decimal/libmpdec/bench_full.c _decimal/libmpdec/bench.c _decimal/libmpdec/constants.c _decimal/libmpdec/context.c _decimal/libmpdec/convolute.c _decimal/libmpdec/crt.c _decimal/libmpdec/difradix2.c _decimal/libmpdec/fnt.c _decimal/libmpdec/fourstep.c _decimal/libmpdec/io.c _decimal/libmpdec/mpalloc.c _decimal/libmpdec/mpsignal.c _decimal/libmpdec/numbertheory.c _decimal/libmpdec/sixstep.c _decimal/libmpdec/transpose.c _decimal/_decimal.c 

_opcode _opcode.c
audioop audioop.c
ossaudiodev ossaudiodev.c

_posixshmem -I$(srcdir)/Modules/_multiprocessing _multiprocessing/posixshmem.c -lrt

_lsprof _lsprof.c rotatingtree.c

_testbuffer _testbuffer.c
#_testcapi _testcapimodule.c
_testimportmultiple _testimportmultiple.c
_testmultiphase _testmultiphase.c
_xxsubinterpreters _xxsubinterpretersmodule.c
_xxtestfuzz _xxtestfuzz/_xxtestfuzz.c _xxtestfuzz/fuzzer.c

xxlimited xxlimited.c
xxlimited_35 xxlimited_35.c

#_dbm _dbmmodule.c -lgdbm_compat -DUSE_GDBM_COMPAT

_sqlite3 -I$(srcdir)/Modules/_sqlite _sqlite/module.c _sqlite/connection.c _sqlite/cursor.c _sqlite/cache.c _sqlite/microprotocols.c _sqlite/prepare_protocol.c _sqlite/statement.c _sqlite/util.c _sqlite/row.c _sqlite/sqlite3.c -lpthread -ldl -lsqlite3 

readline readline.c -lreadline -ltermcap


# Modules that should always be present (non UNIX dependent):
array -DPy_BUILD_CORE_MODULE arraymodule.c	# array objects
cmath cmathmodule.c _math.c -DPy_BUILD_CORE_MODULE # -lm # complex math library functions
math mathmodule.c _math.c -DPy_BUILD_CORE_MODULE # -lm # math library functions, e.g. sin()
_contextvars _contextvarsmodule.c  # Context Variables
_struct -DPy_BUILD_CORE_MODULE _struct.c	# binary structure packing/unpacking
_weakref _weakref.c	# basic weak reference support
#_testcapi _testcapimodule.c    # Python C API test module
_testinternalcapi _testinternalcapi.c -I$(srcdir)/Include/internal -DPy_BUILD_CORE_MODULE  # Python internal C API test module
_random _randommodule.c -DPy_BUILD_CORE_MODULE	# Random number generator
_elementtree -I$(srcdir)/Modules/expat -DHAVE_EXPAT_CONFIG_H -DUSE_PYEXPAT_CAPI _elementtree.c	# elementtree accelerator
_pickle -DPy_BUILD_CORE_MODULE _pickle.c	# pickle accelerator
_datetime _datetimemodule.c	# datetime accelerator
_zoneinfo _zoneinfo.c -DPy_BUILD_CORE_MODULE	# zoneinfo accelerator
_bisect _bisectmodule.c	# Bisection algorithms
_heapq _heapqmodule.c -DPy_BUILD_CORE_MODULE	# Heap queue algorithm
_asyncio _asynciomodule.c  # Fast asyncio Future
_json -I$(srcdir)/Include/internal -DPy_BUILD_CORE_BUILTIN _json.c	# _json speedups
_statistics _statisticsmodule.c # statistics accelerator

unicodedata unicodedata.c -DPy_BUILD_CORE_BUILTIN   # static Unicode character database


# Modules with some UNIX dependencies -- on by default:
# (If you have a really backward UNIX, select and socket may not be
# supported...)

fcntl fcntlmodule.c	# fcntl(2) and ioctl(2)
spwd spwdmodule.c		# spwd(3)
grp grpmodule.c		# grp(3)
select selectmodule.c	# select(2); not on ancient System V

# Memory-mapped files (also works on Win32).
mmap mmapmodule.c

# CSV file helper
_csv _csv.c

# Socket module helper for socket(2)
_socket socketmodule.c

# Socket module helper for SSL support; you must comment out the other
# socket line above, and edit the OPENSSL variable:
# _ssl _ssl.c \
#     $(OPENSSL_INCLUDES) $(OPENSSL_LDFLAGS) \
#     -lssl -lcrypto
#_hashlib _hashopenssl.c \
#     $(OPENSSL_INCLUDES) $(OPENSSL_LDFLAGS) \
#     -lcrypto

# To statically link OpenSSL:
_ssl _ssl.c \
     $(OPENSSL_INCLUDES) $(OPENSSL_LDFLAGS) \
     -l:libssl.a -Wl,--exclude-libs,libssl.a \
     -l:libcrypto.a -Wl,--exclude-libs,libcrypto.a
     -lssl -lcrypto
_hashlib _hashopenssl.c \
      $(OPENSSL_INCLUDES) $(OPENSSL_LDFLAGS) \
     -l:libcrypto.a -Wl,--exclude-libs,libcrypto.a
     -lcrypto

# The crypt module is now disabled by default because it breaks builds
# on many systems (where -lcrypt is needed), e.g. Linux (I believe).

_crypt _cryptmodule.c # -lcrypt	# crypt(3); needs -lcrypt on some systems


# Some more UNIX dependent modules -- off by default, since these
# are not supported by all UNIX systems:

#nis nismodule.c -lnsl	# Sun yellow pages -- not everywhere
termios termios.c	# Steen Lumholt's termios module
resource resource.c	# Jeremy Hylton's rlimit interface

_posixsubprocess  -DPy_BUILD_CORE_BUILTIN _posixsubprocess.c  # POSIX subprocess module helper

# Multimedia modules -- off by default.
# These don't work for 64-bit platforms!!!
# #993173 says audioop works on 64-bit platforms, though.
# These represent audio samples or images as strings:

#audioop audioop.c	# Operations on audio samples


# Note that the _md5 and _sha modules are normally only built if the
# system does not have the OpenSSL libs containing an optimized version.

# The _md5 module implements the RSA Data Security, Inc. MD5
# Message-Digest Algorithm, described in RFC 1321.

_md5 md5module.c


# The _sha module implements the SHA checksum algorithms.
# (NIST's Secure Hash Algorithms.)
_sha1 sha1module.c
_sha256 sha256module.c -DPy_BUILD_CORE_BUILTIN
_sha512 sha512module.c -DPy_BUILD_CORE_BUILTIN
_sha3 _sha3/sha3module.c

# _blake module
_blake2 _blake2/blake2module.c _blake2/blake2b_impl.c _blake2/blake2s_impl.c

# The _tkinter module.
#
# The command for _tkinter is long and site specific.  Please
# uncomment and/or edit those parts as indicated.  If you don't have a
# specific extension (e.g. Tix or BLT), leave the corresponding line
# commented out.  (Leave the trailing backslashes in!  If you
# experience strange errors, you may want to join all uncommented
# lines and remove the backslashes -- the backslash interpretation is
# done by the shell's "read" command and it may not be implemented on
# every system.

## *** Always uncomment this (leave the leading underscore in!):
#_tkinter _tkinter.c tkappinit.c -DWITH_APPINIT \
## *** Uncomment and edit to reflect where your Tcl/Tk libraries are:
#	-L/usr/local/lib \
## *** Uncomment and edit to reflect where your Tcl/Tk headers are:
#	-I/usr/local/include \
## *** Uncomment and edit to reflect where your X11 header files are:
#	-I/usr/X11R6/include \
## *** Or uncomment this for Solaris:
#	-I/usr/openwin/include \
## *** Uncomment and edit for Tix extension only:
#	-DWITH_TIX -ltix8.1.8.2 \
## *** Uncomment and edit for BLT extension only:
#	-DWITH_BLT -I/usr/local/blt/blt8.0-unoff/include -lBLT8.0 \
## *** Uncomment and edit for PIL (TkImaging) extension only: (See http://www.pythonware.com/products/pil/ for more info)
#	-DWITH_PIL -I../Extensions/Imaging/libImaging  tkImaging.c \
## *** Uncomment and edit for TOGL extension only:
#	-DWITH_TOGL togl.c \
## *** Uncomment and edit to reflect your Tcl/Tk versions:
#	-ltk8.2 -ltcl8.2 \
## *** Uncomment and edit to reflect where your X11 libraries are:
#	-L/usr/X11R6/lib \
## *** Or uncomment this for Solaris:
#	-L/usr/openwin/lib \
## *** Uncomment these for TOGL extension only:
#	-lGL -lGLU -lXext -lXmu \
## *** Uncomment for AIX:
#	-lld \
## *** Always uncomment this; X11 libraries to link with:
#	-lX11

# Lance Ellinghaus's syslog module
syslog syslogmodule.c		# syslog daemon interface


# Curses support, requiring the System V version of curses, often
# provided by the ncurses library.  e.g. on Linux, link with -lncurses
# instead of -lcurses).

#_curses _cursesmodule.c -lcurses -ltermcap -DPy_BUILD_CORE_MODULE
## Wrapper for the panel library that's part of ncurses and SYSV curses.
#_curses_panel _curses_panel.c -lpanel -lncurses

# Modules that provide persistent dictionary-like semantics.  You will
# probably want to arrange for at least one of them to be available on
# your machine, though none are defined by default because of library
# dependencies.  The Python module dbm/__init__.py provides an
# implementation independent wrapper for these; dbm/dumb.py provides
# similar functionality (but slower of course) implemented in Python.

#_dbm _dbmmodule.c 	# dbm(3) may require -lndbm or similar

# Anthony Baxter's gdbm module.  GNU dbm(3) will require -lgdbm:

_gdbm _gdbmmodule.c -I/usr/local/include -L/usr/local/lib -lgdbm


# Helper module for various ascii-encoders
binascii binascii.c

# Andrew Kuchling's zlib module.
# This require zlib 1.1.3 (or later).
# See http://www.gzip.org/zlib/
zlib zlibmodule.c -I$(prefix)/include -L$(exec_prefix)/lib -lz

# Interface to the Expat XML parser
# More information on Expat can be found at www.libexpat.org.
#
pyexpat expat/xmlparse.c expat/xmlrole.c expat/xmltok.c pyexpat.c -I$(srcdir)/Modules/expat -DHAVE_EXPAT_CONFIG_H -DXML_POOR_ENTROPY -DUSE_PYEXPAT_CAPI

# Hye-Shik Chang's CJKCodecs

# multibytecodec is required for all the other CJK codec modules
_multibytecodec cjkcodecs/multibytecodec.c

_codecs_cn cjkcodecs/_codecs_cn.c
_codecs_hk cjkcodecs/_codecs_hk.c
_codecs_iso2022 cjkcodecs/_codecs_iso2022.c
_codecs_jp cjkcodecs/_codecs_jp.c
_codecs_kr cjkcodecs/_codecs_kr.c
_codecs_tw cjkcodecs/_codecs_tw.c

# Example -- included for reference only:
# xx xxmodule.c

# Another example -- the 'xxsubtype' module shows C-level subtyping in action
xxsubtype xxsubtype.c

# Uncommenting the following line tells makesetup that all following modules
# are not built (see above for more detail).
#
#*disabled*
#
#_sqlite3 _tkinter _curses pyexpat
#_codecs_jp _codecs_kr _codecs_tw unicodedata
