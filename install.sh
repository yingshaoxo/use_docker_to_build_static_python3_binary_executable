architecture=$(uname -m)
if [[ $architecture != *'x86_64'* ]]; then
    echo 'We only support x86_64 for now, if you want other archtecture python3.10, you could built it yourself by using the dockerfile.'
    echo 'Or if you are busy, you can check if this repository has a folder called data_i386 or not, if there has one, you simply use it.'
    echo 'git clone https://gitlab.com/yingshaoxo/use_docker_to_build_static_python3_binary_executable.git'
    exit
fi

if which git >/dev/null; then
    echo "git is installed."
else
    echo "git is not installed. Installation Failed."
    exit 0
fi

if which curl >/dev/null; then
    echo "curl is installed."
else
    echo "curl is not installed. Installation Failed."
    exit 0
fi



mkdir -p /home/python
cd /home/python

git clone https://gitlab.com/yingshaoxo/use_docker_to_build_static_python3_binary_executable.git
cd use_docker_to_build_static_python3_binary_executable

mkdir -p /home/python/static_python_3.10

cp -fr data/Python-3.10.4/* /home/python/static_python_3.10/
#cp -fr data/Python-3.10.4/Lib /home/python/static_python_3.10/
#cp -fr data/Python-3.10.4/Modules /home/python/static_python_3.10/
#cp -fr data/Python-3.10.4/python /home/python/static_python_3.10/
#cp -fr data/Python-3.10.4/pybuilddir.txt /home/python/static_python_3.10/
#cp -fr data/Python-3.10.4/build /home/python/static_python_3.10/

cd /home/python/static_python_3.10
chmod 777 python

rm -fr /bin/python3.10
ln -s /home/python/static_python_3.10/python /bin/python3.10
chmod 777 /bin/python3.10

rm -fr /usr/bin/python3.10
ln -s /home/python/static_python_3.10/python /usr/bin/python3.10
chmod 777 /usr/bin/python3.10

echo 'Done. Now use python3.10 --version to see what you got.'
